#!/bin/bash -l

###########################################
# Go 1.5
###########################################

environment() {
  export GOPATH=/home/ggerman/go
  export GOBIN=/home/ggerman/go/go/bin
  export GOROOT=/home/ggerman/go/go
  export GOROOT_BOOTSTRAP=/home/ggerman/go/go1.4
  export NDK_ROOT=/home/ggerman/android-ndk-r10e
  export CC_FOR_TARGET=/home/ggerman/android-ndk-r10e/bin/arm-linux-androideabi-gcc
  export GOOS=android
  export GOARCH=arm
  export GOARM=7
  export CGO_ENABLED=1
  export PATH=$PATH:/home/ggerman/android-sdk-linux/platform-tools:$GOPATH/go/bin

  # export GOARCH="amd64"
  # export GOHOSTARCH="amd64"
  # export GOTOOLDIR=/home/ggerman/go/go/pkg/tool/linux_amd64
  # export CC=gcc
}

environment

# $NDK_ROOT/build/tools/make-standalone-toolchain.sh --install-dir=$NDK_ROOT --arch=arm --toolchain=arm-linux-androideabi-4.9 --platform=android-21
# cd $GOROOT/src; CC_FOR_TARGET=$NDK_ROOT/bin/arm-linux-androideabi-gcc GOOS=android GOARCH=arm GOARM=7 CGO_ENABLED=1 ./make.bash

