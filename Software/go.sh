#!/bin/bash -l

###########################################
# Go 1.5
###########################################

environment() {
  export GOARCH=amd64
  export GOPATH=/home/ggerman/go
  export GOBIN=/home/ggerman/go/go/bin
  export GOROOT=/home/ggerman/go/go
  export GOROOT_BOOTSTRAP=/home/ggerman/go/go1.4
  export PATH=$PATH:$GOPATH/go/bin

  unset NDK_ROOT

  export GOTOOLDIR=/home/ggerman/go/go/pkg/tool/linux_amd64
  export CC="gcc"
  export GOGCCFLAGS="-fPIC -m64 -pthread -fmessage-length=0"
  export CXX=g++
  export CGO_ENABLED=1
  
  unset CC_FOR_TARGET
}

environment

# $NDK_ROOT/build/tools/make-standalone-toolchain.sh --install-dir=$NDK_ROOT --arch=arm --toolchain=arm-linux-androideabi-4.9 --platform=android-21
# cd $GOROOT/src; CC_FOR_TARGET=$NDK_ROOT/bin/arm-linux-androideabi-gcc GOOS=android GOARCH=arm GOARM=7 CGO_ENABLED=1 ./make.bash

