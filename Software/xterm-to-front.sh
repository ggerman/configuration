#!/bin/bash
TITLE_XTERM=$(wmctrl -l -x | grep xterm | awk '{print $1}')
echo $TITLE_XTERM

wmctrl -i -R $TITLE_XTERM || xterm -e screen
